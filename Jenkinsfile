pipeline{
    agent any
    environment {
        TF_HOME = '/usr/local/bin/terraform'
        TF_var_file =  'variables.tf'
        TF_Backend_file = 'State-Backend.tf'
        TF_WKSPACE    =  'KEY1-DC-SETUP'  // ex:- CLIENT1-DC-SETUP
        STORAGE_ACCOUNT_RG  = 'cprasad_resources' // ex:- MphRxInfraStates
        STORAGE_ACCOUNT_NAME  = 'cprasadstorageterraform' // ex:- mphrxterraformstatefiles
        STORAGE_CONTAINER_NAME  = 'key-test' // ex:- democlient-terraform-states
        STATE_FILE_NAME  = 'terraform-state-file-key1.tfstate' //ex:- newterraform.tfstate
        DC_TF_var_file = 'Infra-Deployment/DC-SETUP/DC_variables.tf'

    }

stages {
        stage('Preparing Terraform Config'){
        steps {
                  echo "Preparing Terraform"
                    withCredentials([azureServicePrincipal(
                    credentialsId: 'Azure_Service_Principle',
                    subscriptionIdVariable: 'ARM_SUBSCRIPTION_ID',
                    clientIdVariable: 'ARM_CLIENT_ID',
                    clientSecretVariable: 'ARM_CLIENT_SECRET',
                    tenantIdVariable: 'ARM_TENANT_ID'
                ), string(credentialsId: 'Storage_Account_SAS_Token', variable: 'ARM_ACCESS_KEY'),
                   string(credentialsId: 'Storage_Account_Subscription_Id', variable: 'STORAGE_SUBSCRIPTION_ID'),
                   string(credentialsId: 'AKS_WORKER_SSH_PUB_KEY', variable: 'AKS_WORKER_SSH_KEY'), 
                   string(credentialsId: 'Storage_Account_Tenant_Id', variable: 'STORAGE_TENANT_ID')]) {

                           // above variables will be exporetd as env variable for this stage only . can be checked via env command as below
                     sh """
                        sed -i "s/PUT_YOUR_AZURE_SUB_ID/${ARM_SUBSCRIPTION_ID}/g" $TF_var_file
                        sed -i "s/PUT_YOUR_AZURE_SUB_ID/${STORAGE_SUBSCRIPTION_ID}/g" $TF_Backend_file
                        sed -i "s/PUT_YOUR_AZURE_TENANT_ID/${ARM_TENANT_ID}/g" $TF_var_file
                        sed -i "s/PUT_YOUR_AZURE_TENANT_ID/${STORAGE_TENANT_ID}/g" $TF_Backend_file
                        sed -i "s/PUT_YOUR_AZURE_SECRET_CLIENT_ID/${ARM_CLIENT_ID}/g" $TF_var_file
                        sed -i "s/PUT_YOUR_AZURE_CLIENT_SECRET/${ARM_CLIENT_SECRET}/g" $TF_var_file
                        sed -i "s/PUT_STORAGE_ACCOUNT_RG/${STORAGE_ACCOUNT_RG}/g" $TF_Backend_file
                        sed -i "s/PUT_STORAGE_ACCOUNT_NAME/${STORAGE_ACCOUNT_NAME}/g" $TF_Backend_file
                        sed -i "s/PUT_BLOB_CONTAINER_NAME/${STORAGE_CONTAINER_NAME}/g" $TF_Backend_file
                        sed -i "s/PUT_NAME_FOR_TERRAFORM_STATE_FILE/${STATE_FILE_NAME}/g" $TF_Backend_file
			sed -i "s%PUT_AKS_WORKERER_SSH_PUB_KEY%${AKS_WORKER_SSH_KEY}%g" $DC_TF_var_file

                        """
                                             }}}

        stage('Terraform Init'){
        steps {
               echo "Initialising Terraform"
                withCredentials([string(credentialsId: 'Storage_Account_SAS_Token', variable: 'ARM_ACCESS_KEY')]) {
                       sh """
                        $TF_HOME  init -input=false
                       """
                                                }}}

        stage('Terraform Validate'){
        steps {
               echo "Validating Config..."
                       sh """
                        $TF_HOME  validate
                        $TF_HOME  fmt -recursive
                       """
                                                }}                  

        stage('Terraform Workspace'){
        steps {
               echo "Creating Required Workpsace"
               withCredentials([string(credentialsId: 'Storage_Account_SAS_Token', variable: 'ARM_ACCESS_KEY')]) { 
                     sh """ 
                        $TF_HOME workspace select $TF_WKSPACE || $TF_HOME workspace new $TF_WKSPACE 
                        """ 
                                        }}}

        stage('Terraform Plan'){
        steps {
                     echo "Creating Infra Plan"
                withCredentials([string(credentialsId: 'Storage_Account_SAS_Token', variable: 'ARM_ACCESS_KEY')]) {
                     sh """
                         $TF_HOME plan
                         """ 
                                         }}}
       stage(' Approval to Create Infra resources'){
       steps {
                timeout(time: 5, unit: 'MINUTES') {
                input (message: "Deploy the infrastructure?")
                                        }}}

       stage('Terraform Apply'){
       steps {
               echo "Creating Infra ..."
               withCredentials([string(credentialsId: 'Storage_Account_SAS_Token', variable: 'ARM_ACCESS_KEY')]) {
                    sh """
                      $TF_HOME apply -auto-approve
                       """
                                        }}}


       stage(' Approval to Destroy Infra resources'){
       steps {
                timeout(time: 5, unit: 'MINUTES') {
                input (message: "Destroy the infrastructure?")
                                        }}}

       stage('Terraform Destroy'){
       steps {
               echo "Creating Infra ..."
               withCredentials([string(credentialsId: 'Storage_Account_SAS_Token', variable: 'ARM_ACCESS_KEY')]) {
                    sh """
                      $TF_HOME destroy -auto-approve
                       """
                                        }}}

}

post {
    always {
        deleteDir()
        dir("${env.WORKSPACE}@tmp") {
            deleteDir()
        }
        dir("${env.WORKSPACE}@script") {
            deleteDir()
        }
        dir("${env.WORKSPACE}@script@tmp") {
            deleteDir()
        }
    }
}

}

