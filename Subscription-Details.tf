// using az login .

provider "azurerm" {
  features {}
  subscription_id = var.AZURE_SUB_ID
  client_id       = var.AZURE_SECRET_CLIENT_ID
  client_secret   = var.AZURE_CLIENT_SECRET
  tenant_id       = var.AZURE_TENANT_ID

}

