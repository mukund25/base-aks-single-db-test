###################################### RG
module "RG-CREATION" {
  source      = "../modules/RG"
  count       = length(var.PROD_DC_RGs)
  RG-name     = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${var.PROD_DC_RGs[count.index]}-${var.location_type}"
  RG-location = var.DC_location
}

##################################### VNET 
module "VNET-CREATION" {
  source          = "../modules/VNET"
  DC_vnet_name    = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}"
  DC_vnet_address = var.DC_VNET_ADDRESS
  VNET-RG         = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}"
  location        = var.DC_location

  depends_on = [
    module.RG-CREATION,
  ]
}

#################################### SUBNET 
module "SUBNET-CREATION" {
  source            = "../modules/SUBNET"
  count             = length(var.PROD_DC_RGs)
  DC_vnet_name      = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}"
  SUBNET-RG         = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}"
  DC_SUBNET_NAME    = var.PROD_DC_RGs[count.index]  == "NETWORK" ? "GatewaySubnet" : "${var.CLIENT_NAME}-${var.SETUP_TYPE}-SUBNET-${var.PROD_DC_RGs[count.index]}-${var.location_type}"
  DC_SUBNET_ADDRESS = var.DC_SUBNET_ADDRESS[count.index]

  depends_on = [
    module.VNET-CREATION,
  ]
}

#################################### NSG CREATION
module "NSG-CREATION" {
  source       = "../modules/NSG-CREATION"
  count        = length(var.PROD_DC_RGs)
  count1       = var.PROD_DC_RGs[count.index] == "NETWORK" ? 0 : 1
  RG-location  = var.DC_location
  DC_vnet_name = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}"
  NSG-NAME     = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-NSG-SUBNET-${var.PROD_DC_RGs[count.index]}-${var.location_type}"
  NSG-RG       = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${var.PROD_DC_RGs[count.index]}-${var.location_type}"

  depends_on = [
    module.SUBNET-CREATION,
  ]
}

###################################  NSG ASSOCIATION 
module "NSG-ASSOC" {
  source                    = "../modules/NSG-ASSOC"
  count                     = length(var.PROD_DC_RGs)
  count1                    = var.PROD_DC_RGs[count.index] == "NETWORK" ? 0 : 1
  network_security_group_id = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${var.PROD_DC_RGs[count.index]}-${var.location_type}/providers/Microsoft.Network/networkSecurityGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-NSG-SUBNET-${var.PROD_DC_RGs[count.index]}-${var.location_type}"
  subnet_id                 = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/virtualNetworks/${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}/subnets/${var.CLIENT_NAME}-${var.SETUP_TYPE}-SUBNET-${var.PROD_DC_RGs[count.index]}-${var.location_type}"
  depends_on = [
    module.NSG-CREATION,
  ]
}

################################### VM NIC Creation

################################### VM NIC Creation
module "NIC-CREATION" {
  source = "../modules/NIC"

  for_each      = var.VM-DETAILS
  NIC-COUNT     = each.value.Count
  NIC-name      = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-${each.key}"
  NIC-location  = var.DC_location
  NIC-RG        = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${each.value.TYPE}-${var.location_type}"
  NIC-SUBNET-ID = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/virtualNetworks/${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}/subnets/${var.CLIENT_NAME}-${var.SETUP_TYPE}-SUBNET-${each.value.TYPE}-${var.location_type}"

  depends_on = [
    module.NSG-ASSOC,
  ]
}


################################### VM AV Creation
module "AV-CREATION" {
  source = "../modules/AV"

  for_each    = var.VM-DETAILS
  AV-name     = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-AV-${each.key}"
  AV-location = var.DC_location
  AV-RG       = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${each.value.TYPE}-${var.location_type}"

  depends_on = [
    module.NIC-CREATION,
  ]
}

################################### Diagnostic Storage Account  Creation for VMs
module "STRG-ACCNT-CREATION" {
  source = "../modules/STRG_ACCOUNT"

  for_each       = var.VM-DETAILS
  STACC-NAME     = "${each.value.TYPE}dg${var.CLIENT_SHORT_NAME}"
  STACC-RG       = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${each.value.TYPE}-${var.location_type}"
  STACC-location = var.DC_location
  STACC-tier     = "Standard"
  STACC-rep      = "LRS"
  STACC-kind     = "Storage"
  kind           = each.key

  depends_on = [
    module.AV-CREATION,
  ]
}

#################################  AZURE FILE SHARE ( DATA, LOG-BAKUP, DBBACKUP ) ,, also can ve used to create V2 storage
module "AZURE-FILE-SHARE" {
  source = "../modules/FILE-SHARE"
  count  = length(var.File-Shares)
  local_name = "${var.File-Shares[count.index]}"
  STACC-NAME = "sa${var.location_type}"
  STACC-RG   = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-DB-${var.location_type}"    
  STACC-location = var.DC_location
  STACC-rep      = "LRS"
  kind           = "AzureFileShare"

 depends_on = [
    module.STRG-ACCNT-CREATION,
  ]
}
############################################AKS Creation

########################################################################################################
########################################################################################################
########################################################################################################
resource "azurerm_kubernetes_cluster" "aks" {
    name = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-AKS-${var.location_type}"
    resource_group_name = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-AKS-${var.location_type}"
    location            = var.DC_location
    dns_prefix =  "${var.CLIENT_NAME}-${var.SETUP_TYPE}-AKS-${var.location_type}"
    kubernetes_version = var.kubernetes_version
    #private_dns_zone_id     = azurerm_private_dns_zone.example.id
    private_cluster_enabled = var.PrivateAKS

    default_node_pool {
      name = "default"
      node_count = var.node_count
      vm_size = var.node_vm_size       #"Standard_A2_v2"
      type = "VirtualMachineScaleSets"
      vnet_subnet_id = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/virtualNetworks/${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}/subnets/${var.CLIENT_NAME}-${var.SETUP_TYPE}-SUBNET-AKS-${var.location_type}"
    }

  identity {
    type = "SystemAssigned"
  }


    linux_profile {
        admin_username = var.admin_user
        ssh_key {
            key_data = var.ssh_key
        }
    }

    network_profile {
      network_plugin = var.network_plugin
      load_balancer_sku = var.load_balancer_sku
      network_policy     = var.network_policy
      service_cidr       = var.service_cidr
      dns_service_ip     = var.dns_service_ip
      docker_bridge_cidr = var.docker_bridge_cidr
    }
 depends_on = [
    module.VM-CREATION,
  ]
}
########################################################################################################
########################################################################################################
########################################################################################################
########################################################################################################




################################### VM Creation
module "VM-CREATION" {
  source = "../modules/VM"

  for_each = var.VM-DETAILS

  VM-COUNT                                = each.value.Count
  VM-NAME                                 = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-${each.value.TYPE}-${each.value.SNAME}-${var.DC_LOCATION_SHORTFORM}"
  VM-location                             = var.DC_location
  VM-RG                                   = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${each.value.TYPE}-${var.location_type}"
  VM-NIC-ID                               = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${each.value.TYPE}-${var.location_type}/providers/Microsoft.Network/networkInterfaces/${var.CLIENT_NAME}-${var.SETUP_TYPE}-${each.key}-nic"
  vm_size                                 = each.value.SIZE
  image-publisher                         = each.value.image-publisher
  image-offer                             = each.value.image-offer
  image-sku                               = each.value.image-sku
  image-versn                             = each.value.image-versn
  os-disk-name                            = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-${each.value.TYPE}-${each.value.SNAME}-${var.DC_LOCATION_SHORTFORM}"
  os-caching                              = "ReadWrite"
  os-create-option                        = each.value.os-create-option
  os-managed-disk-type                    = each.value.os-managed-disk-type
  profile-computer-name                   = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-${each.value.TYPE}-${each.value.SNAME}-${var.DC_LOCATION_SHORTFORM}"
  profile-admin-username                  = each.value.profile-admin-username
  profile-admin-password                  = each.value.profile-admin-password
  profile-disable-password-authentication = "false"
  VM-AV-ID                                = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${each.value.TYPE}-${var.location_type}/providers/Microsoft.Compute/availabilitySets/${var.CLIENT_NAME}-${var.SETUP_TYPE}-AV-${each.key}"
  DIAG-STGACC-URI                         = "https://${each.value.TYPE}dg${var.CLIENT_SHORT_NAME}.blob.core.windows.net/"

  depends_on = [
    module.STRG-ACCNT-CREATION,
  ]
}

module "Data-Disks" {
    source = "../modules/MANAGED-DISK"
    count     =  length(var.DATA_DISKs)
    local_name =  "${var.DATA_DISKs[count.index]}"
    name      =  "${var.CLIENT_NAME}-${var.SETUP_TYPE}"
    location  =  var.DC_location
    resource_group_name  =  "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-"
    storage_account_type  = "Standard_LRS"
    create_option         = "Empty"

  depends_on = [
    module.VM-CREATION,
  ]
}
#########################################  DATA DISKS ATTACH TO VM

module "Attach-Data-Disks" {
    source = "../modules/ATTACH-MANAGED-DISK"
    count     =  length(var.DATA_DISKs)
    local_name =  "${var.DATA_DISKs[count.index]}"
    rg_prefix  = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-"
    vm_name_prefix = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-"
    DC_LOCATION_SHORTFORM = var.DC_LOCATION_SHORTFORM
    caching    = "ReadWrite"

  depends_on = [
    module.Data-Disks,
  ]
}

################################### LB Creation

################################################ LB FE IP CREATION
module "loadbalancerIP" {
  source = "../modules/LB/LB_IP"

  for_each = var.LB-DETAILS
  TYPE     = each.value.TYPE
  LB_NAME  = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-LB"
  LB-RG    = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}"
  location = var.DC_location
  IP_alloc = each.value.IP_alloc

  depends_on = [
    module.Attach-Data-Disks,
  ]
}

############################################## LB + BKND POOL ASSOCIATION
module "loadbalancer" {
  source = "../modules/LB/LB_CREATE"

  for_each          = var.LB-DETAILS
  LB-RG             = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}"
  LB_NAME           = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-${each.key}-${each.value.TYPE}-lb"
  TYPE              = each.value.TYPE
  ports             = each.value.PORTs
  IP_alloc          = each.value.IP_alloc
  location          = var.DC_location
  PUB-IP-ID         = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/publicIPAddresses/${var.CLIENT_NAME}-${var.SETUP_TYPE}-LB-PUBLIC-IP"
  SUBNET            = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/virtualNetworks/${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}/subnets/${var.CLIENT_NAME}-${var.SETUP_TYPE}-SUBNET-${each.value.SUBNET}-${var.location_type}"
  LB-ID-PREFIX      = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/loadBalancers/"
  LB-BE-POLL-PREFIX = each.key
  NODE_COUNT        = each.value.NODE_COUNT
  NIC-name          = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-${each.key}"
  NIC-ID            = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-${each.value.SUBNET}-${var.location_type}/providers/Microsoft.Network/networkInterfaces/${var.CLIENT_NAME}-${var.SETUP_TYPE}-${each.key}-nic"

  depends_on = [
    module.loadbalancerIP,
  ]
}

########################################### VPN  GATEWAY

module "Gateways" {
  source = "../modules/GATEWAYs"

  for_each                    = var.GATEWAYs
  Required                    = each.value.Required
  resource_group_name         = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}"
  location                    = var.DC_location
  virtual_network_name        = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}"
  subnet_name                 = "GatewaySubnet"
  gw_ip_name                  = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-GW-${upper(each.key)}-PUBIP"
  public_ip_allocation_method = "Dynamic"
  public_ip_sku               = "Basic"
  vpn_gateway_name            = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-GW-${upper(each.key)}-${var.location_type}"
  vpn_type                    = each.value.vpn_type
  gateway_type                = each.value.gateway_type
  generation                  = each.value.vpn_gw_generation
  enable_active_active        = each.value.enable_active_active
  vpn_gw_sku                  = each.value.vpn_gw_sku
  expressroute_sku            = each.value.expressroute_sku
  enable_bgp                  = "false"
  public_ip_address_id        = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/publicIPAddresses/${var.CLIENT_NAME}-${var.SETUP_TYPE}-GW-${upper(each.key)}-PUBIP"
  subnet_id                   = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/virtualNetworks/${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}/subnets/GatewaySubnet"

  depends_on = [
    module.loadbalancer,
  ]
}

#################################### Local Network gateway + connection creation
module "localNtwrkGtws" {
  source                     = "../modules/GATEWAYs/LNGTWY"
  count                      = length(var.local_networks)
  name                       = "LNGW-${var.local_networks[count.index].local_gw_name}-TO-${var.CLIENT_NAME}-${var.SETUP_TYPE}-${var.location_type}"
  resource_group_name        = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}"
  location                   = var.DC_location
  gateway_address            = var.local_networks[count.index].local_gateway_address
  address_space              = var.local_networks[count.index].local_address_space
  conn_name                  = "CONN-${var.local_networks[count.index].local_gw_name}-TO-${var.CLIENT_NAME}-${var.SETUP_TYPE}-${var.location_type}"
  conn_type                  = "IPsec"
  virtual_network_gateway_id = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/virtualNetworkGateways/${var.CLIENT_NAME}-${var.SETUP_TYPE}-GW-VPN-${var.location_type}"
  local_network_gateway_id   = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/localNetworkGateways/LNGW-${upper(var.local_networks[count.index].local_gw_name)}-TO-${var.CLIENT_NAME}-${var.SETUP_TYPE}-${var.location_type}"

  depends_on = [
    module.Gateways,
  ]
}
#############################################################################
