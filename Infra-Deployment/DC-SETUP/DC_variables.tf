########### IMP :-  Variable Name should  not be changed ( Modules defination depend on them )  , but yes values can be changes as per requirement 

variable "DC_location" {
  description = "azure location to comission PROD DC Infra"
}

variable "SETUP_TYPE" {}
variable "DATA_DISKs" {}
variable "File-Shares" {}

variable "DC_LOCATION_SHORTFORM" {
  description = " DC LOCATION SHORT NAME"
}

variable "location_type" {
  description = " location type"
}

variable "AZURE_SUB_ID" {
  description = "SubID"
}

variable "CLIENT_NAME" {
  description = "Client Name"
}

variable "CLIENT_SHORT_NAME" {
  description = "Client SHORT Name"
}

variable "PROD_DC_RGs" {
  description = "Name of All RGs"
}

variable "DC_VNET_ADDRESS" {
  description = "Primary VDC VNET NAME"
}


variable "DC_SUBNET_ADDRESS" {
  description = "Primary VDC SUBNET ADDRESS"
}



############################# VM Creation variables

variable "VM-DETAILS" {

  type = map(any)

  default = {

    "MONGO-MYSQL" = {
      "Count"                  = 1
      "TYPE"                   = "DB"
      "SNAME"                  = "mg-ms"
      "SIZE"                   = "Standard_D2s_v3"
      "image-publisher"        = "OpenLogic"
      "image-offer"            = "CentOS"
      "image-sku"              = "7.7"
      "image-versn"            = "latest"
      "os-create-option"       = "FromImage"
      "os-managed-disk-type"   = "Standard_LRS"
      "profile-admin-username" = "azureadmin"
      "profile-admin-password" = "P$s$wO5d1234!"
    }
  }
}


############################# LB Creation variables

variable "LB-DETAILS" {

  type = map(any)

  default = {
    "MINERVA" = {
      "NODE_COUNT" = 0
      "TYPE"       = "public"
      "PORTs"      = ["80", "443"]
      "IP_alloc"   = "Static"
      "SUBNET"     = "FE"
    }
  }
}


###########################  VPN +  Express Route  Gateway Creation

variable "GATEWAYs" {

  type = map(any)

  default = {
    "VPN" = {
      "Required"             = "true"
      "vpn_type"             = "RouteBased"
      "vpn_gw_sku"           = "Basic"
      "expressroute_sku"     = ""
      "vpn_gw_generation"    = "Generation1"
      "enable_active_active" = "false"
      "gateway_type"         = "Vpn"
    }

  }
}


###########################  Local Network Gateway +  Ipsec Connection Creation

variable "local_networks" {

  type = list(object({ local_gw_name = string, local_gateway_address = string, local_address_space = list(string), }))

  default = [
    {
      "local_gw_name"         = "mphrx-atl"
      "local_gateway_address" = "208.65.104.226"
      "local_address_space"   = ["10.151.0.0/16"]
    },

    {
      "local_gw_name"         = "mphrx-mia"
      "local_gateway_address" = "67.215.180.130"
      "local_address_space"   = ["10.150.0.0/16"]
    },

    {
      "local_gw_name"         = "mphrx-GGN"
      "local_gateway_address" = "180.151.87.90"
      "local_address_space"   = ["172.25.30.0/24", "192.168.0.0/23"]
    },

    {
      "local_gw_name"         = "mphrx-openvpn"
      "local_gateway_address" = "13.71.7.179"
      "local_address_space"   = ["10.20.1.0/24", "172.20.1.0/24"]
    },

    {
      "local_gw_name"         = "mphrx-openvpn1"
      "local_gateway_address" = "13.71.112.14"
      "local_address_space"   = ["10.20.2.0/24", "172.20.2.0/24"]
    },
  ]
}



###############################AKS Variable

// #####################################################   AKS Properties   #######################################

variable "AKS_Name" {
  type        = string
  description = "AKS name"
        default = "AKS-CPRASAD"
}
variable "kubernetes_version" {
  type = string
  description = "k8's version"
  default = "1.20.9"
}

variable "PrivateAKS" {
        type = bool
        default = true
}
variable "admin_user"{
  type = string
  description = "username for linux_profile"
  default = "cprasad"
}


variable "ssh_key" {
   description = "ssh_key for admin_user"
   default= "PUT_AKS_WORKERER_SSH_PUB_KEY"
}

variable "node_vm_size"{
  type = string
  description = "VM size of the cluster node"
  default = "Standard_A2_v2"
}

variable "node_count"{
  type = number
  description = "No of node VMs aks cluster"
  default = 2
}
variable "acr_name" {
  type        = string
  description = "ACR name"
  default = "K8S_ACR_TR"
}
variable "profile-admin-password" {
  type = string
  default = "MphRx@123"
}




variable "network_plugin" {
  type = string
  default = "azure"
}
variable "load_balancer_sku" {
  type = string
  default = "Standard"
}
variable "network_policy" {
  type = string
  default = "azure"
}
variable "service_cidr" {
  type = string
  default = "10.50.5.0/24"
}
variable "dns_service_ip" {
  type = string
  default = "10.50.5.10"
}
variable "docker_bridge_cidr" {
  type = string
  default = "172.17.0.1/16"
}





// ################################################################################################################
// ################################################################################################################
// ################################################################################################################
// ################################################################################################################




