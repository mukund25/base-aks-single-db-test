// For creating  Resource Groups

resource "azurerm_resource_group" "RG" {

  name     = var.RG-name
  location = var.RG-location
}

output "RG" {
  value = azurerm_resource_group.RG.id
}
