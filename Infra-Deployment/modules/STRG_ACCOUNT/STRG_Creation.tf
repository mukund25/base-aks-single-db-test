
resource "azurerm_storage_account" "storageaccount" {

  name                     = lower(var.STACC-NAME)
  resource_group_name      = var.STACC-RG
  location                 = var.STACC-location
  account_tier             = var.STACC-tier
  account_replication_type = var.STACC-rep
  account_kind             = var.STACC-kind

  tags = {
    environment = var.kind
  }

}

