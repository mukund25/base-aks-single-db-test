

resource "azurerm_virtual_machine" "VM" {
  count                            = var.VM-COUNT
  name                             = "${lower(var.VM-NAME)}${count.index + 1}"
  location                         = var.VM-location
  availability_set_id              = var.VM-AV-ID
  resource_group_name              = var.VM-RG
  network_interface_ids            = ["${var.VM-NIC-ID}${count.index + 1}"]
  vm_size                          = var.vm_size
  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = var.image-publisher
    offer     = var.image-offer
    sku       = var.image-sku
    version   = var.image-versn
  }

  storage_os_disk {
    name              = "${lower(var.os-disk-name)}${count.index + 1}_OSDisk"
    caching           = var.os-caching
    create_option     = var.os-create-option
    managed_disk_type = var.os-managed-disk-type
  }

  os_profile {
    computer_name  = "${lower(var.profile-computer-name)}${count.index + 1}"
    admin_username = var.profile-admin-username
    admin_password = var.profile-admin-password
    custom_data    = file("cloud_init_add_user.txt")
  }

  os_profile_linux_config {
    disable_password_authentication = var.profile-disable-password-authentication
  }

  boot_diagnostics {
    enabled     = true
    storage_uri = lower(var.DIAG-STGACC-URI)
  }

}




