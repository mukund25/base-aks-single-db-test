resource "azurerm_network_interface" "VM_NIC" {
  count               = var.NIC-COUNT
  name                = "${var.NIC-name}-nic${count.index + 1}"
  location            = var.NIC-location
  resource_group_name = var.NIC-RG

  ip_configuration {
    name                          = "${var.NIC-name}-ip${count.index + 1}"
    subnet_id                     = var.NIC-SUBNET-ID
    private_ip_address_allocation = "Dynamic"
    
  }
}


