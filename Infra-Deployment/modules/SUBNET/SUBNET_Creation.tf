###############   working 
# Create Subnet
resource "azurerm_subnet" "az_subnets" {

  name                 = var.DC_SUBNET_NAME
  address_prefixes     = [var.DC_SUBNET_ADDRESS]
  resource_group_name  = var.SUBNET-RG
  virtual_network_name = var.DC_vnet_name

}


output "Subnets" {
    value = azurerm_subnet.az_subnets.id
    }

