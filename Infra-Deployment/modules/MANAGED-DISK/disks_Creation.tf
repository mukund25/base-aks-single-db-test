locals {
  local_name   =  split(":", "${var.local_name}")
}
 

resource "azurerm_managed_disk" "amd" {
  count                =  local.local_name[2]
  name                 = "${var.name}-DATA_DISK-${local.local_name[0]}-${local.local_name[1]}${count.index + 1}"
  location             = var.location
  resource_group_name  = "${var.resource_group_name}${local.local_name[0]}-DC"
  storage_account_type = local.local_name[1] == "HDD" ? "Standard_LRS" : "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "${local.local_name[3]}"

}

