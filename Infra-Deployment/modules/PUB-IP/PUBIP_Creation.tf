

resource "azurerm_public_ip" "example" {
  count               = var.PUBIP-COUNT
  name                = "${var.name}${count.index + 1}"
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = var.allocation_method

}
