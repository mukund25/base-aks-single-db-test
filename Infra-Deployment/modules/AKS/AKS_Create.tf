
resource "azurerm_kubernetes_cluster" "aks" {
    name = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-AKS-${var.location_type}"
    resource_group_name = "${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-AKS-${var.location_type}"
    location            = var.DC_Location
    dns_prefix =  "${var.CLIENT_NAME}-${var.SETUP_TYPE}-AKS-${var.location_type}"
    kubernetes_version = var.kubernetes_version
    #private_dns_zone_id     = azurerm_private_dns_zone.example.id
    private_cluster_enabled = var.PrivateAKS

    default_node_pool {
      name = "default"
      node_count = var.node_count
      vm_size = var.node_vm_size       #"Standard_A2_v2"
      type = "VirtualMachineScaleSets"
      vnet_subnet_id = "/subscriptions/${var.AZURE_SUB_ID}/resourceGroups/${var.CLIENT_NAME}-${var.SETUP_TYPE}-RG-NETWORK-${var.location_type}/providers/Microsoft.Network/virtualNetworks/${var.CLIENT_NAME}-${var.SETUP_TYPE}-VNET-${var.location_type}/subnets/${var.CLIENT_NAME}-${var.SETUP_TYPE}-SUBNET-AKS-${var.location_type}"
    }

  identity {
    type = "SystemAssigned"
  }


    linux_profile {
        admin_username = var.admin_user
        ssh_key {
            key_data = var.ssh_key
        }
    }

    network_profile {
      network_plugin = var.network_plugin
      load_balancer_sku = var.load_balancer_sku
      network_policy     = var.network_policy
      service_cidr       = var.service_cidr
      dns_service_ip     = var.dns_service_ip
      docker_bridge_cidr = var.docker_bridge_cidr
    }

}

