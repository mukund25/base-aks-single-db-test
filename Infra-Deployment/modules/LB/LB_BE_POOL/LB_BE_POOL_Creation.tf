
resource "azurerm_network_interface_backend_address_pool_association" "aznicassoc" {
  network_interface_id    = azurerm_network_interface.example.id
  ip_configuration_name   = "testconfiguration1"
  backend_address_pool_id = "${var.LB-ID-PREFIX}${upper(var.LB_NAME)}/backendAddressPools/${var.LB-BE-POLL-PREFIX}-BE-POOL"
}
