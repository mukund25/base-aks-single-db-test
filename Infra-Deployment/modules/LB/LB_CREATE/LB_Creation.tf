
resource "azurerm_lb" "azlb" {
  name                = upper(var.LB_NAME)
  resource_group_name = var.LB-RG
  location            = var.location
  sku                 = "Basic"

  frontend_ip_configuration {
    name                          = "${upper(var.LB_NAME)}-IP-NAME"
    public_ip_address_id          = var.TYPE == "public" ? "${var.PUB-IP-ID}" : ""
    subnet_id                     = var.TYPE == "private" ? var.SUBNET : ""
    private_ip_address_allocation = "Dynamic"
  }
}


resource "azurerm_lb_backend_address_pool" "azlb" {
  name            = "${var.LB-BE-POLL-PREFIX}-BE-POOL"
  loadbalancer_id = "${var.LB-ID-PREFIX}${upper(var.LB_NAME)}"

  depends_on = [
    azurerm_lb.azlb,
  ]
}

resource "azurerm_lb_probe" "azlb" {
  count               = length(var.ports)
  name                = "Probe_${element(var.ports, count.index)}"
  resource_group_name = var.LB-RG
  loadbalancer_id     = "${var.LB-ID-PREFIX}${upper(var.LB_NAME)}"
  protocol            = "Tcp"
  port                = element(var.ports, count.index)
  interval_in_seconds = 5
  number_of_probes    = 2

  depends_on = [
    azurerm_lb_backend_address_pool.azlb,
  ]
}

resource "azurerm_lb_rule" "azlb" {
  count                          = length(var.ports)
  name                           = "Rule_${element(var.ports, count.index)}"
  resource_group_name            = var.LB-RG
  loadbalancer_id                = "${var.LB-ID-PREFIX}${upper(var.LB_NAME)}"
  protocol                       = "Tcp"
  frontend_port                  = element(var.ports, count.index)
  backend_port                   = element(var.ports, count.index)
  frontend_ip_configuration_name = "${upper(var.LB_NAME)}-IP-NAME" 
  enable_floating_ip             = false
  backend_address_pool_id        = "${var.LB-ID-PREFIX}${upper(var.LB_NAME)}/backendAddressPools/${var.LB-BE-POLL-PREFIX}-BE-POOL"
  idle_timeout_in_minutes        = 5
  probe_id                       = "${var.LB-ID-PREFIX}${upper(var.LB_NAME)}/probes/Probe_${element(var.ports, count.index)}"

  depends_on = [
    azurerm_lb_probe.azlb,
  ]
}


resource "azurerm_network_interface_backend_address_pool_association" "aznicassoc" {
  count                   = var.NODE_COUNT
  network_interface_id    = "${var.NIC-ID}${count.index + 1}"
  ip_configuration_name   = "${var.NIC-name}-ip${count.index + 1}"
  backend_address_pool_id = "${var.LB-ID-PREFIX}${upper(var.LB_NAME)}/backendAddressPools/${var.LB-BE-POLL-PREFIX}-BE-POOL"
  depends_on = [
    azurerm_lb_rule.azlb,
  ]

}

