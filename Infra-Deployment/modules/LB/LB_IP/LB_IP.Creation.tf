resource "azurerm_public_ip" "azlb" {
  count               = var.TYPE == "public" ? 1 : 0
  name                = "${upper(var.LB_NAME)}-${upper(var.TYPE)}-IP"
  resource_group_name = var.LB-RG
  location            = var.location
  allocation_method   = var.IP_alloc
  sku                 = "Basic"
}
