locals {
  local_name   =  split(":", "${var.local_name}")
}


resource "azurerm_storage_account" "File_Share_Storageaccount" {

  name                     = "${lower(local.local_name[0])}${lower(var.STACC-NAME)}"
  resource_group_name      = var.STACC-RG
  location                 = var.STACC-location
  account_tier             = "${local.local_name[1]}"
  account_replication_type = var.STACC-rep
  account_kind             = "${local.local_name[2]}"

  tags = {
    environment = var.kind
  }

}

resource "azurerm_storage_share" "azure_file_share" {
  name                 = "${local.local_name[0]}"
  storage_account_name = "${lower(local.local_name[0])}${lower(var.STACC-NAME)}"
  quota                = local.local_name[3]
  
depends_on = [
   azurerm_storage_account.File_Share_Storageaccount,
]
}

