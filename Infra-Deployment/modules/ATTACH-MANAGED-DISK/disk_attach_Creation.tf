locals {
  local_name   =  split(":", "${var.local_name}")
}
 

resource "azurerm_virtual_machine_data_disk_attachment" "avmdda" {

  count                =  local.local_name[4]
  managed_disk_id      =  "${var.rg_prefix}${local.local_name[0]}-DC/providers/Microsoft.Compute/disks/${var.vm_name_prefix}DATA_DISK-${local.local_name[0]}-${local.local_name[1]}${count.index + 1}"
  virtual_machine_id   =  local.local_name[0] == "DB" ? "${var.rg_prefix}${local.local_name[0]}-DC/providers/Microsoft.Compute/virtualMachines/${lower(var.vm_name_prefix)}${lower(local.local_name[0])}-mg-ms-${lower(var.DC_LOCATION_SHORTFORM)}${count.index + 1}" : "${var.rg_prefix}${local.local_name[0]}-DC/providers/Microsoft.Compute/virtualMachines/${lower(var.vm_name_prefix)}${lower(local.local_name[0])}-min-${lower(var.DC_LOCATION_SHORTFORM)}${count.index + 1}"
   lun                = "${local.local_name[5]}"
  caching             = "${var.caching}"

}
