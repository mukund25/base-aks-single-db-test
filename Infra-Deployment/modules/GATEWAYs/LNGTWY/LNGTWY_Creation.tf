
resource "azurerm_local_network_gateway" "localgw" {
  name                = upper(var.name)
  resource_group_name = var.resource_group_name
  location            = var.location
  gateway_address     = var.gateway_address
  address_space       = var.address_space
}


resource "random_password" "password" {
  length           = 32
  special          = true
  override_special = "$!%"
}

resource "azurerm_virtual_network_gateway_connection" "az-hub-onprem" {
  name                = upper(var.conn_name)
  location            = var.location
  resource_group_name = var.resource_group_name
  type                = var.conn_type
  shared_key          = random_password.password.result

  virtual_network_gateway_id = var.virtual_network_gateway_id
  local_network_gateway_id   = var.local_network_gateway_id

  depends_on = [
    azurerm_local_network_gateway.localgw,
    random_password.password
  ]
}

