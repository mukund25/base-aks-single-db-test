
resource "azurerm_network_security_group" "DC-NSG" {

  count               = var.count1
  name                = var.NSG-NAME
  resource_group_name = var.NSG-RG
  location            = var.RG-location
}

output "NSGs" {
    value = azurerm_network_security_group.DC-NSG[*].id
    }

