
resource "azurerm_subnet_network_security_group_association" "NSG_ASSOC" {

  count = var.count1

  network_security_group_id = var.network_security_group_id
  subnet_id                 = var.subnet_id

}

output "NSG_ASSOC_ID" {
value = azurerm_subnet_network_security_group_association.NSG_ASSOC[*].id

}

