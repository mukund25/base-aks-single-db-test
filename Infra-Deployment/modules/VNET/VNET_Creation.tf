################################## PROD-DC  - VNET

resource "azurerm_virtual_network" "vnet" {

  name                = var.DC_vnet_name
  resource_group_name = var.VNET-RG
  location            = var.location
  address_space       = [var.DC_vnet_address]
}

output "vnet" {
  value = azurerm_virtual_network.vnet.id
}

