resource "azurerm_availability_set" "AV" {
  name                         = var.AV-name
  location                     = var.AV-location
  resource_group_name          = var.AV-RG
  platform_fault_domain_count  = 2
  platform_update_domain_count = 5
}

output "AVs" {
  value = azurerm_availability_set.AV.id
}
