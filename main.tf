// Name of client for which comissioning is to be done
variable "CLIENT_NAME" {
  description = "Client Name"
  default     = "KEY1" // in Capital letters ex-: CLIENT1
}

variable "DC_Locations" {
  description = "Azure region location for Infra comissioning "
  type        = map(string)
  default = {
    DC = "centralindia"  // ex-: westindia
  }
}

####################################   Comission DC Resources

module "DC-Infra-Deployment" {
  source                = "./Infra-Deployment/DC-SETUP"
  count                 = terraform.workspace == "${var.CLIENT_NAME}-DC-SETUP" ? 1 : 0 # matches current workspace and expect to be like "CLIENT1-DC-SETUP"
  AZURE_SUB_ID          = var.AZURE_SUB_ID
  CLIENT_NAME           = var.CLIENT_NAME
  location_type         = "DC"
  SETUP_TYPE            = "POC"    // ex:- TESTING,UAT,DEV,PERF,PILOT,PROD,POC etc
  DC_location           = var.DC_Locations["DC"]
  DC_LOCATION_SHORTFORM = "cenind"  // ex:- souind,wesind,cenind   
  CLIENT_SHORT_NAME     = "cpr"  // ex:- cl1,pop,ca,nh

  PROD_DC_RGs           = ["NETWORK", "AKS", "DB", "SERVICE", "MISC"]    //Resource group and subnest will be created using the same variable
  DC_VNET_ADDRESS       = "10.71.0.0/16"   // ex:- 10.19.13.0/24 
  DC_SUBNET_ADDRESS     = ["10.71.0.0/24", "10.71.1.0/24", "10.71.2.0/24","10.71.3.0/24","10.71.4.0/24"]
  File-Shares           = ["dbbackupkey:Standard:StorageV2:50"]
   
  DATA_DISKs            = ["DB:HDD:1:32:1:0", "DB:PSSD:1:128:1:1"]  //VM_TYPE:HDD/PSSD:DISK_COUNT:SIZE:ATTACHING-VMS:LUN
}

###################################################### Default maodule
module "default" {
  source = "./Infra-Deployment/default"
  count  = terraform.workspace == "default" ? 1 : 0 // if current workspace is default/local
}

output "Current_WorkSpace_Check" {
  value = module.default
}
