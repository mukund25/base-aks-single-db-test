terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 2.62" // provider version check ( azure )
    }
  }

  required_version = ">= 0.15.0" // Terraform version check 
}