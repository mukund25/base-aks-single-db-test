terraform {
  backend "azurerm" {
    resource_group_name  = "PUT_STORAGE_ACCOUNT_RG"    // storage account RG
    storage_account_name = "PUT_STORAGE_ACCOUNT_NAME"    // storage account name
    container_name       = "PUT_BLOB_CONTAINER_NAME"    // blob container name
    key                  = "PUT_NAME_FOR_TERRAFORM_STATE_FILE"    //  terraform state file name ( prefix in-case of workspaces )
    subscription_id      = "PUT_YOUR_AZURE_SUB_ID"    // azure sub ID
    tenant_id            = "PUT_YOUR_AZURE_TENANT_ID"    // azure tenant ID

  }
}
