variable "AZURE_SUB_ID" {
  description = "azure Subscription id"
  default     = "PUT_YOUR_AZURE_SUB_ID"
}

variable "AZURE_TENANT_ID" {
  description = "azure Tenant id"
  default     = "PUT_YOUR_AZURE_TENANT_ID"
}

variable "AZURE_SECRET_CLIENT_ID" {
  description = "azure Client id"
  default     = "PUT_YOUR_AZURE_SECRET_CLIENT_ID"
}

variable "AZURE_CLIENT_SECRET" {
  description = "azure Client Secret"
  default     = "PUT_YOUR_AZURE_CLIENT_SECRET"
}
